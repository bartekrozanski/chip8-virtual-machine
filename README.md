# Chip8VM

Chip 8 virtual machine implemented in Haskell working in terminal. For display and input handling [ncurses](https://hackage.haskell.org/package/ncurses) was used.

# Usage
```
./[executable] [programFile.c8]
```
# Screenshots
<img src="screenshots/screenshot1.png" width="600" height="300" />
<img src="screenshots/screenshot2.png" width="600" height="300" />
<img src="screenshots/screenshot3.png" width="600" height="300" />

# Building this project
This project uses [stack](https://docs.haskellstack.org/en/stable/README/) for handling project building.
Clone the repository and run `stack build`.
