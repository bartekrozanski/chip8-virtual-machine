module Main where

import Chip8
import Chip8.Types
import Chip8.Manipulation
import UI.NCurses
import Data.Array
import System.Environment
import Numeric

main :: IO ()
main = do
    args <- getArgs
    chip <- loadFileToMemory cleanChip (head args)
    runCurses $ do
        setEcho False
        w <- defaultWindow
        mainLoop w chip

cycleTime :: Integer
cycleTime = 5

keyboardMap :: Maybe Event -> Maybe Int
keyboardMap Nothing = Nothing
keyboardMap (Just (EventCharacter x)) = if null result then Nothing else Just (fst $ head result) 
  where 
    result = readHex [x]
keyboardMap _ = Nothing

updateChipKeyboard :: Chip8 -> Maybe Int -> Chip8
updateChipKeyboard chip Nothing = chipWithClearedInput
  where
    chipWithClearedInput = chip {keyboardState = cleanKeyboardState}
updateChipKeyboard chip (Just val) = setKey chipWithClearedInput $ fromIntegral val
  where
    chipWithClearedInput = chip {keyboardState = cleanKeyboardState}

-- mainLoop :: Window -> Chip8 -> Curses ()
-- mainLoop w chip = 
--     getEvent w (Just cycleTime) >>= 
--     (return ((updateChipKeyboard chip) . keyboardMap)) >>=
--     return emulateCycle >>=
--     mainLoop w

mainLoop :: Window -> Chip8 -> Curses ()
mainLoop w chip = do
    ev <- getEvent w (Just cycleTime)
    maybeInt <- return $ keyboardMap ev
    chipWithInput <- return $ updateChipKeyboard chip maybeInt
    newChip <- return $ emulateCycle chipWithInput
    renderChip w chip
    -- waitFor w (\ev -> ev == EventCharacter 'q' || ev == EventCharacter 'Q')
    mainLoop w newChip

renderChip :: Window -> Chip8 -> Curses ()
renderChip w chip = do
    updateWindow w $ do
        clear
        mapM_ (\((x,y), val) -> moveCursor (fromIntegral (y + 3)) (fromIntegral x) >> if val then drawString "X" else return ()) (assocs (framebuffer chip))
        moveCursor 40 0
        drawString $ show chip 
    render

waitFor :: Window -> (Event -> Bool) -> Curses ()
waitFor w p = loop where
    loop = do
        ev <- getEvent w $ Just 16
        case ev of
            Nothing -> return ()
            Just ev' -> if p ev' then return () else loop