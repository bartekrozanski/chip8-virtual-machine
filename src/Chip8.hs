module Chip8
where

import Chip8.Types
import Chip8.Manipulation
import Data.Bits
import Data.Word
import Data.Char
import Data.Array ((//))
import System.IO
cleanChip :: Chip8
cleanChip = Chip8 {
    registers = cleanRegisters,
    instructionSet = chip8InstructionSet,
    memory = cleanMemoryWithFonts,
    stack = cleanStack,
    framebuffer = cleanFramebuffer,
    keyboardState = cleanKeyboardState,
    rng = cleanRng
}

chip8InstructionSet :: InstructionSet
chip8InstructionSet opcode = 
  case deconstructed of
        (0x0, 0x0, 0xE, 0x0) -> op_00E0
        (0x0, 0x0, 0xE, 0xE) -> op_00EE
        (0x0,_,_,_) -> undefined --not necessary for most programs
        (0x1, _, _, _) -> op_1NNN
        (0x2, _, _, _) -> op_2NNN
        (0x3, _, _, _) -> op_3XNN
        (0x4, _, _, _) -> op_4XNN
        (0x5, _, _, 0x0) -> op_5XY0
        (0x6, _, _, _) -> op_6XNN
        (0x7, _, _, _) -> op_7XNN
        (0x8, _, _, 0x0) -> op_8XY0
        (0x8, _, _, 0x1) -> op_8XY1
        (0x8, _, _, 0x2) -> op_8XY2
        (0x8, _, _, 0x3) -> op_8XY3
        (0x8, _, _, 0x4) -> op_8XY4
        (0x8, _, _, 0x5) -> op_8XY5
        (0x8, _, _, 0x6) -> op_8XY6
        (0x8, _, _, 0x7) -> op_8XY7
        (0x8, _, _, 0xE) -> op_8XYE
        (0x9, _, _, 0x0) -> op_9XY0
        (0xA, _, _, _) -> op_ANNN
        (0xB, _, _, _) -> op_BNNN
        (0xC, _, _, _) -> op_CXNN
        (0xD, _, _, _) -> op_DXYN
        (0xE, _, 0x9, 0xE) -> op_EX9E
        (0xE, _, 0xA, 0x1) -> op_EXA1
        (0xF, _, 0x0, 0x7) -> op_FX07
        (0xF, _, 0x0, 0xA) -> op_FX0A
        (0xF, _, 0x1, 0x5) -> op_FX15
        (0xF, _, 0x1, 0x8) -> op_FX18
        (0xF, _, 0x1, 0xE) -> op_FX1E
        (0xF, _, 0x2, 0x9) -> op_FX29
        (0xF, _, 0x3, 0x3) -> op_FX33
        (0xF, _, 0x5, 0x5) -> op_FX55
        (0xF, _, 0x6, 0x5) -> op_FX65
        _ -> error "Unknown opcode"
    where
      deconstructed = deconstructOpcode opcode

op_00E0 :: Operation
op_00E0 chip _ = chip {framebuffer = cleanFramebuffer}

op_00EE :: Operation
op_00EE chip _ = setPC newChip targetAddress
  where
    (newChip, targetAddress) = popFromStack chip

op_1NNN :: Operation
op_1NNN chip (_,n1,n2,n3) = setPC chip (joinBits3 n1 n2 n3)

op_2NNN :: Operation
op_2NNN chip (_,n1,n2,n3) = setPC (pushOnStack chip (getPC chip)) targetAddress
  where
    targetAddress = joinBits3 n1 n2 n3

op_3XNN :: Operation
op_3XNN chip (_,x,n1,n2) = 
  if xVal == nn
    then increasePC chip 2
    else chip
  where
    nn = joinBits2 n1 n2
    xVal = getGPR chip x

op_4XNN :: Operation
op_4XNN chip (_,x,n1,n2) = 
  if xVal /= nn
    then increasePC chip 2
    else chip
  where
    nn = joinBits2 n1 n2
    xVal = getGPR chip x

op_5XY0 :: Operation
op_5XY0 chip (_,x,y,_) =
  if xVal == yVal
    then increasePC chip 2
    else chip
  where
    xVal = getGPR chip x
    yVal = getGPR chip y


op_6XNN :: Operation
op_6XNN chip (_,x,n1,n2) = setGPR chip x (joinBits2 n1 n2)

op_7XNN :: Operation
op_7XNN chip (_,x,n1,n2) = setGPR chip x (xVal + joinBits2 n1 n2)
  where
    xVal = getGPR chip x

op_8XY0 :: Operation
op_8XY0 chip (_,x,y,_) = setGPR chip x (getGPR chip y)

op_8XY1 :: Operation
op_8XY1 chip (_,x,y,_) = setGPR chip x ((getGPR chip x) .|. (getGPR chip y))

op_8XY2 :: Operation
op_8XY2 chip (_,x,y,_) = setGPR chip x ((getGPR chip x) .&. (getGPR chip y))

op_8XY3 :: Operation
op_8XY3 chip (_,x,y,_) = setGPR chip x ((getGPR chip x) `xor` (getGPR chip y))
 
op_8XY4 :: Operation
op_8XY4 chip (_,x,y,_)
  | overflowSum > 255 = setGPR newChip 0xF 1
  | otherwise = setGPR newChip 0xF 0
    where
      xVal = getGPR chip x
      yVal = getGPR chip y
      overflowSum :: Int
      overflowSum = fromIntegral xVal + fromIntegral yVal
      newChip = setGPR chip x (xVal + yVal)

op_8XY5 :: Operation
op_8XY5 chip (_,x,y,_)
  | borrowDiff < 0 = setGPR newChip 0xF 0
  | otherwise = setGPR newChip 0xF 1
    where
      xVal = getGPR chip x
      yVal = getGPR chip y
      borrowDiff :: Int
      borrowDiff = fromIntegral xVal - fromIntegral yVal
      newChip = setGPR chip x (xVal - yVal)

op_8XY6 :: Operation
op_8XY6 chip (_,x,y,_) = setGPR (setGPR chip x newxVal) 0xF leastSignificantBitX
  where
    xVal = getGPR chip x
    leastSignificantBitX = xVal .&. (0x1)
    newxVal = shiftR xVal 1

op_8XY7 :: Operation
op_8XY7 chip (_,x,y,_)
  | borrowDiff < 0 = setGPR newChip 0xF 0
  | otherwise = setGPR newChip 0xF 1
    where
      xVal = getGPR chip x
      yVal = getGPR chip y
      borrowDiff :: Int
      borrowDiff = fromIntegral yVal - fromIntegral xVal
      newChip = setGPR chip x (yVal - xVal)

op_8XYE :: Operation
op_8XYE chip (_,x,y,_) = setGPR (setGPR chip x newxVal) 0xF mostSignificantBitX
  where
    xVal = getGPR chip x
    mostSignificantBitX = xVal .&. (0x80)
    newxVal = shiftL xVal 1

op_9XY0 :: Operation
op_9XY0 chip (_,x,y,_) =
  if xVal == yVal
    then chip
    else increasePC chip 2
  where
    xVal = getGPR chip x
    yVal = getGPR chip y

op_ANNN :: Operation
op_ANNN chip (_,n1,n2,n3) = setAddressReg chip addrVal
  where
    addrVal = joinBits3 n1 n2 n3

op_BNNN :: Operation
op_BNNN chip (_,n1,n2,n3) = setAddressReg chip targetAddr
  where
    addrVal = joinBits3 n1 n2 n3
    v0 = getGPR chip 0x0
    targetAddr :: Address
    targetAddr = fromIntegral v0 + addrVal


-- here is random number needed! for now lets use some constant
op_CXNN :: Operation
op_CXNN chip (_,x,n1,n2) = setGPR newChip x (joined .&. randomNum)
  where
    joined = joinBits2 n1 n2
    (newChip, randomNum) = extractRandomNumber chip

op_DXYN :: Operation
op_DXYN chip (_,x,y,n)
  | checkIfSpriteUnsetsPixels chip sprite = setGPR newChip 0xF 0x1
  | otherwise = setGPR newChip 0xF 0x0
  where
    xVal = getGPR chip x
    yVal = getGPR chip y
    sprite = extractSpriteFromMemory chip (xVal, yVal) (n+1) 
    newChip = drawSprite chip sprite

op_EX9E :: Operation
op_EX9E chip (_,x,_,_)
  | clicked = increasePC chip 2
  | otherwise = chip
  where
    clicked = getKey chip (getGPR chip x)

op_EXA1 :: Operation
op_EXA1 chip (_,x,_,_)
  | notClicked = increasePC chip 2
  | otherwise = chip
  where
    notClicked = not $ getKey chip (getGPR chip x)

op_FX07 :: Operation
op_FX07 chip (_,x,_,_) = setGPR chip x (getDelayTimer chip) 

op_FX0A :: Operation --wait for key press
op_FX0A chip _ = chip

op_FX15 :: Operation
op_FX15 chip (_,x,_,_) = setDelayTimer chip (getGPR chip x)

op_FX18 :: Operation --set sound timer
op_FX18 chip _ = chip

op_FX1E :: Operation
op_FX1E chip (_,x,_,_) = setAddressReg chip newAddr
  where
    oldAddr = getAddressReg chip
    xVal = getGPR chip x
    newAddr = oldAddr + fromIntegral xVal

op_FX29 :: Operation
op_FX29 chip (_,x,_,_) = setAddressReg chip (fromIntegral xVal * 5)
  where
    xVal = getGPR chip x 


op_FX33 :: Operation
op_FX33 chip (_,x,_,_) = chip {memory = newMem}
  where
    l = getAddressReg chip
    xVal = getGPR chip x
    hundreds = xVal `div` 100
    tens = xVal `div` 10 `mod` 10
    ones = xVal `mod` 100 `mod` 10
    newMem = memory chip // [(l, hundreds), (l + 1, tens), (l + 2, ones)]

op_FX55 :: Operation
op_FX55 chip (_,x,_,_) = dumpRegisters chip x

op_FX65 :: Operation
op_FX65 chip (_,x,_,_) = fillRegisters chip x

emulateCycle :: Chip8 -> Chip8
emulateCycle = decrementDelayTimer . executeOperation . decodeOpcode . fetchOpcode

loadFileToMemory :: Chip8 -> FilePath -> IO Chip8
loadFileToMemory chip filepath = do
  handle <- openBinaryFile filepath ReadMode
  program <- hGetContents handle
  return $ chip {memory = memory chip // (zip [0x200..] $ map (fromIntegral . ord) program)}

