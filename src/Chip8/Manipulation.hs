module Chip8.Manipulation

where

import Data.Array
import Data.Word
import Data.Bits
import Chip8.Types


joinBits3 :: Byte ->  Byte -> Byte -> Word16
joinBits3 a b c = leftPart .|. middlePart .|. rightPart
  where
    base :: Word16
    base = 0x0
    leftPart = shiftL (base .|. fromIntegral a) 8
    middlePart = shiftL (base .|. fromIntegral b) 4
    rightPart = shiftL (base .|. fromIntegral c) 0

joinBits2 :: Byte ->  Byte -> Byte
joinBits2 a b = leftPart .|. rightPart
  where
    base :: Word8
    base = 0x0
    leftPart = shiftL (base .|. fromIntegral a) 4
    rightPart = shiftL (base .|. fromIntegral b) 0

registersModifyGPRs :: Registers -> (GPRs -> GPRs) -> Registers
registersModifyGPRs regs f = regs {vX = newvX}
  where
    newvX = f . vX $ regs

registersSetGPR :: Byte -> Byte -> Registers -> Registers
registersSetGPR reg val regs = registersModifyGPRs regs (//[(reg,val)])

registersGetGPR :: Byte -> Registers -> Byte
registersGetGPR reg regs = vX regs ! reg

registersSetAddressReg :: Address -> Registers -> Registers
registersSetAddressReg addr regs = regs {addressRegister = addr}

getAddressReg :: Chip8 -> Address
getAddressReg =  addressRegister . registers

getMemoryVal :: Chip8 -> Address -> Byte
getMemoryVal chip addr = memory chip ! addr

setAddressReg :: Chip8 -> Address -> Chip8
setAddressReg chip addr = chip {registers = newRegs}
  where
   newRegs = registersSetAddressReg addr (registers chip)

setGPR :: Chip8 -> Byte -> Byte-> Chip8
setGPR chip reg val =  chip {registers = newRegs}
  where
    newRegs = (registersSetGPR reg val) . registers $ chip

getGPR :: Chip8 -> Byte -> Byte
getGPR chip reg = (registersGetGPR reg) . registers $ chip


registersIncreasePC :: Registers -> Address -> Registers
registersIncreasePC regs n = regs {programCounter = programCounter regs + n}

registersSetPC :: Registers -> Address -> Registers
registersSetPC regs n = regs {programCounter = n}

increasePC :: Chip8 -> Address -> Chip8
increasePC chip n = chip {registers = registersIncreasePC (registers chip) n}

setPC :: Chip8 -> Address -> Chip8
setPC chip addr = chip {registers = registersSetPC (registers chip) addr}

getPC :: Chip8 -> Address
getPC = programCounter . registers

getKey :: Chip8 -> Word8 -> Bool
getKey chip which = keyboardState chip ! which

setKey :: Chip8 -> Word8 -> Chip8
setKey chip which = chip { keyboardState = keyboardState chip // [(which, True)]}  

fetchOpcode :: Chip8 -> (Chip8, Opcode)
fetchOpcode chip = (newChip, (shiftL firstPart 8) .|. secondPart)
  where
    currentAddress = programCounter $ registers chip
    mem = memory chip
    firstPart = fromIntegral $ mem ! currentAddress
    secondPart = fromIntegral $ mem ! (currentAddress + 1)
    newChip = increasePC chip 2

decodeOpcode :: (Chip8, Opcode) -> (Chip8, Operation, DeconstructedOp)
decodeOpcode (chip, op) = (chip, (instructionSet chip) op, deconstructOpcode op)

executeOperation :: (Chip8, Operation, DeconstructedOp) -> Chip8
executeOperation (chip, operation, deconstructedOp) = 
  operation chip deconstructedOp

-- getHexVal number [group of four bits from right]
-- getHexVal 0xAB32 2 = B 
getHexVal :: Word16 -> Int -> Byte
getHexVal n s = fromIntegral $ shiftR n (s * 4) .&. 0xF

deconstructOpcode :: Opcode -> (Byte, Byte, Byte, Byte)
deconstructOpcode op = (first, second, third, fourth)
  where
    first = getHexVal op 3 
    second = getHexVal op 2
    third = getHexVal op 1
    fourth = getHexVal op 0

pushOnStack :: Chip8 -> Address -> Chip8
pushOnStack chip addr = chip {stack = newStack}
  where
    newStack = addr:(stack chip)

popFromStack :: Chip8 -> (Chip8, Address)
popFromStack chip = (chip {stack = xs}, val)
  where
    val:xs = stack chip

extractRandomNumber :: Chip8 -> (Chip8, Word8)
extractRandomNumber chip = (newChip,randomVal)
  where
    (randomVal:[], xs) = splitAt 1 (rng chip)
    newChip = chip {rng = xs}

extractSpriteFromMemory :: Chip8 -> PixelCoords -> Word8 -> [Pixel]
extractSpriteFromMemory chip coords height = recFunc chip addrRegVal coords (height-1)
  where
    addrRegVal = getAddressReg chip
    recFunc :: Chip8 -> Address -> PixelCoords -> Word8 -> [Pixel]
    recFunc chip addr (x,y) n
      | n > 0 = (transformByteToPixels (getMemoryVal chip addr) (x,y)) ++ (recFunc chip (addr+1) (x,y+1) (n-1)) 
      | otherwise = []

transformByteToPixels :: Byte -> PixelCoords -> [Pixel]
transformByteToPixels byte (x0,y0) = recFunc byte (x0 + 7,y0) 0
  where
  recFunc :: Byte -> PixelCoords -> Int -> [Pixel]
  recFunc val (x,y) n
    | n < 8 = ((x,y), testBit val n) : recFunc val (x-1,y) (n+1)
    | n == 8 = []  

drawSprite :: Chip8 -> [Pixel] -> Chip8
drawSprite chip sprite = chip {framebuffer = accum xor (framebuffer chip) sprite}

checkIfSpriteUnsetsPixels :: Chip8 -> [Pixel] -> Bool
checkIfSpriteUnsetsPixels chip = any (checkIfPixelUnsetsBit chip)

checkIfPixelUnsetsBit :: Chip8 -> Pixel -> Bool
checkIfPixelUnsetsBit chip (coords, val) =
  (framebuffer chip ! coords) && val


prepareRegsDump :: Chip8 -> Byte -> Byte -> [(Address,Byte)]
prepareRegsDump chip currReg topReg
  | currReg > topReg = []
  | otherwise = (offsetAddr, regVal) : prepareRegsDump chip (currReg + 1) topReg
  where
    offsetAddr = getAddressReg chip + fromIntegral currReg
    regVal = getGPR chip currReg
  
dumpRegisters :: Chip8 -> Byte -> Chip8
dumpRegisters chip topReg = chip {memory = memory chip // regsDump}
  where
    regsDump = prepareRegsDump chip 0x0 topReg

prepareRegsFill :: Chip8 -> Byte -> Byte -> [(Byte,Byte)]
prepareRegsFill chip currReg topReg
  | currReg > topReg = []
  | otherwise = (currReg, targetRegVal) : prepareRegsFill chip (currReg + 1) topReg
  where
    currentAddr = getAddressReg chip + fromIntegral currReg
    targetRegVal = getMemoryVal chip currentAddr 
  
fillRegisters :: Chip8 -> Byte -> Chip8
fillRegisters chip topReg = chip {registers = newRegs}
  where
    oldRegs = registers chip
    newRegs = oldRegs {vX = vX oldRegs // regsFill}
    regsFill = prepareRegsFill chip 0x0 topReg

setDelayTimerRegisters :: Registers -> Byte -> Registers
setDelayTimerRegisters regs val = regs {delayTimer = val}

setDelayTimer :: Chip8 -> Byte -> Chip8
setDelayTimer chip val = chip {registers = setDelayTimerRegisters (registers chip) val}

decrementDelayTimer :: Chip8 -> Chip8
decrementDelayTimer chip
  | currVal == 0 = chip
  | otherwise = setDelayTimer chip (currVal - 1)
  where
    currVal = delayTimer . registers $ chip

getDelayTimer :: Chip8 -> Byte
getDelayTimer chip = delayTimer . registers $ chip