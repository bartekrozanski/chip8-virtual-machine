module Chip8.Types

where

import Data.Array
import Data.Word
import System.Random
type Address = Word16
type Byte = Word8
type Memory = Array Address Byte
type Operation = Chip8 -> DeconstructedOp -> Chip8
type DeconstructedOp = (Byte, Byte, Byte, Byte)
type Opcode = Word16
type InstructionSet = Opcode -> Operation
type GPRs = Array Word8 Word8 --general purpose registers
type Stack = [Address]
type Framebuffer = Array PixelCoords Color
type PixelCoords = (Word8,Word8)
type Pixel = (PixelCoords, Color)
type Color = Bool
type KeyboardState = Array Word8 Bool

data Registers = Registers{
    programCounter :: Address,
    stackPointer :: Address,
    addressRegister :: Address,
    delayTimer :: Byte,
    vX :: GPRs
}

data Chip8 = Chip8 {
    registers :: Registers,
    instructionSet :: InstructionSet,
    memory :: Memory,
    stack :: Stack,
    framebuffer :: Framebuffer,
    keyboardState :: KeyboardState,
    rng :: [Word8]
}

instance Show Registers where
  show regs = pc ++ "\n" ++ vx ++ "\n" ++ l ++"\n"++delay ++"\n"
    where
      pc = "PC: " ++ show (programCounter regs)
      vx = "VX: " ++ show (vX regs)
      l = "L: " ++ show (addressRegister regs)
      delay = "DT: " ++ show (delayTimer regs)

instance Show Chip8 where
  show chip = (show $ registers chip) ++ "\n" ++ mem
    where
      mem = "RAM: "  ++ show ((take 32 . (drop $ fromIntegral (currAddrPC - 1)) . assocs . memory) chip)
      currAddrPC = programCounter $ registers chip

memorySize :: Address
memorySize = 0xffff

cleanStack :: Stack
cleanStack = []

cleanRng :: [Word8]
cleanRng = randomRs (0,255) $ mkStdGen 123

-- cleanMemory :: Memory
-- cleanMemory = array (0,0xff) [(i,3) | i <- [0..0xff]]
cleanMemory :: Memory
cleanMemory = array (0, memorySize) [(i,0) | i <- [0 ..  memorySize]]

cleanMemoryWithFonts :: Memory
cleanMemoryWithFonts = cleanMemory // 
  (font0 ++ font1 ++ font2 ++ 
  font3 ++ font4 ++ font5 ++ 
  font6 ++ font7 ++ font8 ++ 
  font9 ++ fontA ++ fontB ++
  fontC ++ fontD ++ fontE ++
  fontF)

cleanKeyboardState :: KeyboardState
cleanKeyboardState = array (0x0,0xF) [(i,False) | i <- [0x0..0xF]]

cleanRegisters :: Registers
cleanRegisters = Registers {
    programCounter = 0x0200,
    stackPointer = 0x0EA0,
    addressRegister = 0x0000,
    vX = array (0x0,0xF) [(i,0) | i <- [0x0..0xF]],
    delayTimer = 0x0
}

cleanFramebuffer :: Framebuffer
cleanFramebuffer = 
  array ((0,0), (width, height)) [((x,y), False) | x <- [0..width], y <- [0..height]]
    where
      width = 70
      height = 40


font0 :: [(Address, Word8)]
font0 = [(0x0, 0xf0), (0x1, 0x90), (0x2, 0x90), (0x3, 0x90), (0x04, 0xf0)]

font1 :: [(Address, Word8)]
font1 = [(0x5, 0x20), (0x6, 0x60), (0x7, 0x20), (0x8, 0x20), (0x09, 0x70)]

font2 :: [(Address, Word8)]
font2 = [(0xA, 0xf0), (0xB, 0x10), (0xC, 0xf0), (0xD, 0x80), (0xE, 0xf0)]

font3 :: [(Address, Word8)]
font3 = [(0xf, 0xf0), (0xa0, 0x10), (0xa1, 0xf0), (0xa2, 0x10), (0xa3, 0xf0)]

font4 :: [(Address, Word8)]
font4 = [(0xa4, 0x90), (0xa5, 0x90), (0xa6, 0xf0), (0xa7, 0x10), (0xa8, 0x10)]

font5 :: [(Address, Word8)]
font5 = [(0xa9, 0xf0), (0xaa, 0x80), (0xab, 0xf0), (0xac, 0x10), (0xad, 0xf0)]

font6 :: [(Address, Word8)]
font6 = [(0xae, 0xf0), (0xaf, 0x80), (0xb0, 0xf0), (0xb1, 0x90), (0x0b2, 0xf0)]

font7 :: [(Address, Word8)]
font7 = [(0xb3, 0xf0), (0xb4, 0x10), (0xb5, 0x20), (0xb6, 0x40), (0xb7, 0x40)]

font8 :: [(Address, Word8)]
font8 = [(0xb8, 0xf0), (0xb9, 0x90), (0xba, 0xf0), (0xbb, 0x90), (0xbc, 0xf0)]

font9 :: [(Address, Word8)]
font9 = [(0xbd, 0xf0), (0xbe, 0x90), (0xbf, 0xf0), (0xc0, 0x10), (0xc1, 0xf0)]

fontA :: [(Address, Word8)]
fontA = [(0xc2, 0xf0), (0xc3, 0x90), (0xc4, 0xf0), (0xc5, 0x90), (0xc6, 0x90)]

fontB :: [(Address, Word8)]
fontB = [(0xc7, 0xe0), (0xc8, 0x90), (0xc9, 0xe0), (0xca, 0x90), (0xcb, 0xe0)]

fontC :: [(Address, Word8)]
fontC = [(0xcc, 0xf0), (0xcd, 0x80), (0xce, 0x80), (0xcf, 0x80), (0xd0, 0xf0)]

fontD :: [(Address, Word8)]
fontD = [(0xd1, 0xe0), (0xd2, 0x90), (0xd3, 0x90), (0xd4, 0x90), (0xd5, 0xe0)]

fontE :: [(Address, Word8)]
fontE = [(0xd6, 0xf0), (0xd7, 0x80), (0xd8, 0xf0), (0xd9, 0x80), (0xda, 0xf0)]

fontF :: [(Address, Word8)]
fontF = [(0xdb, 0xf0), (0xdc, 0x80), (0xdd, 0xf0), (0xde, 0x80), (0xdf, 0x80)]